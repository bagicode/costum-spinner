package com.bagicode.myapplication.model

import com.google.gson.annotations.SerializedName

/*
 * Programmer by Robby Dianputra
 * Fungsi kelas : Kelas ini digunakan sebagai item dari get otp
 *
 * Detail dari response otp
 */

data class SignupByPhoneResponse(
    @SerializedName("country_code")
    var countryCode: String,
    @SerializedName("full_hp")
    var fullHp: String,
    @SerializedName("hp")
    var hp: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("messages")
    var messages: String
)