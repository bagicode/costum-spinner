package com.bagicode.myapplication.model

import com.google.gson.annotations.SerializedName

/*
 * Programmer by Robby Dianputra
 * Fungsi kelas : Kelas ini digunakan sebagai item dari bendera
 * Spinner untuk load flag atau bendera sesuai negara
 *
 * Detail dari response list bendera
 */

data class CountryCodeItem(

    @SerializedName("flags")
    var flags: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("iso")
    var iso: String,
    @SerializedName("nicename")
    var nicename: String,
    @SerializedName("phonecode")
    var phonecode: String
)