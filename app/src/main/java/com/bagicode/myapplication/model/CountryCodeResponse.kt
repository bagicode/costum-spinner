package com.bagicode.myapplication.model

/*
 * Programmer by Robby Dianputra
 * Fungsi kelas : Kelas ini digunakan sebagai list dari bendera
 * Spinner untuk load flag atau bendera sesuai negara
 *
 * List dari list bendera
 */

data class CountryCodeResponse(
    val flags : List<CountryCodeItem>)