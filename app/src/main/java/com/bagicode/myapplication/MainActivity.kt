package com.bagicode.myapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.support.design.widget.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import android.support.test.espresso.idling.CountingIdlingResource
import android.util.Log
import com.bagicode.myapplication.adapter.CountryAdapter
import com.bagicode.myapplication.api.ApiService
import com.bagicode.myapplication.model.CountryCodeItem
import java.util.ArrayList;

/*
 * Programmer by Robby Dianputra
 * MainActivty adalah Home Page dari aplikasi ini
 * menggunakan RX
 */

class MainActivity : AppCompatActivity() {
    private var mCountryList: ArrayList<CountryCodeItem>? = null
    private var mAdapter: CountryAdapter? = null

    private lateinit var idlingResource: CountingIdlingResource
    private lateinit var apiService: ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initList()

        mAdapter = CountryAdapter(this, mCountryList!!)
        spinner_countries.setAdapter(mAdapter)

        spinner_countries.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val clickedItem = parent.getItemAtPosition(position) as CountryCodeItem
                val clickedCountryName = clickedItem.nicename
                Toast.makeText(this@MainActivity,
                    "$clickedCountryName selected", Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        })

        btn_register.setOnClickListener {
            val goSignupByPhone = Intent(this, SignupByPhoneActivity::class.java)
            startActivity(goSignupByPhone)
        }


    }

    private fun initList() {
        mCountryList = ArrayList()

        idlingResource = CountingIdlingResource("DATA_LOADER")

        apiService = ApiService.create()

        idlingResource.increment()

        progressBar.hide()
        mCountryList?.add(
            CountryCodeItem(
                "id.svg",
                "100", "ID", "Indonesia", "+62"
            )
        )

        apiService.getCountryCode()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { progressBar.show() }
            .doOnComplete { progressBar.hide() }
            .doOnError { progressBar.hide() }
            .map { it.flags }
            .subscribe(
                { displayQuote(it) },
                { displayError(it) }
            )
    }

    private fun displayQuote(data: List<CountryCodeItem>) {

        var i = 0
        for (i in data.indices ) {
            mCountryList?.add(
                CountryCodeItem(
                    data.get(i).flags,
                    data.get(i).id, data.get(i).iso, data.get(i).nicename, data.get(i).phonecode
                )
            )

            Log.v("tamvan", "ini dia No "+i+" Flag "+ data.get(i).flags)
        }


        idlingResource.decrement()
    }

    private fun displayError(throwable: Throwable?) {
        Snackbar.make(spinner_countries,
            "Failed to display list quote. \n ${throwable?.localizedMessage}",
            Snackbar.LENGTH_LONG).show()

        Log.v("tamvan", "ini dia errornya "+ throwable?.localizedMessage)
    }

    private fun View.show() {
        this.visibility = View.VISIBLE
    }

    private fun View.hide() {
        this.visibility = View.GONE
    }

}
