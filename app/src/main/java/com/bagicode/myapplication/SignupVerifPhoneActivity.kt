package com.bagicode.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_signup_verif_phone.*
import android.support.v4.app.NotificationCompat.getExtras
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.util.Log

/*
 * Programmer by Robby Dianputra
 * SignupByPhone adalah page verif OTP
 * menggunakan RX
 */

class SignupVerifPhoneActivity : AppCompatActivity() {

    val EXTRA_SMS_NO = "extra_sms_no"
    val EXTRA_SMS_MESSAGE = "extra_sms_message"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_verif_phone)

        registerReceiver(broadcastReceiver, IntentFilter("broadCastSMS"));

        /*
         * Cara Biasa
         */
//        val senderNo = intent.getStringExtra(EXTRA_SMS_NO)
//        val senderMessage = intent.getStringExtra(EXTRA_SMS_MESSAGE)
//
//        tv_message.text = "ini dia messagenya "+senderMessage+" dari number "+senderNo
//
//        if (senderMessage.isNullOrBlank()) {
//
//        } else {
//            var messagePositionUD = senderMessage.lastIndexOf("UD-")
//            var messageCompare = senderMessage.substring(messagePositionUD + 3)
//
//            et_message.setText(messageCompare)
//        }
    }

    var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val b = intent.extras
            val senderMessage = b!!.getString(EXTRA_SMS_MESSAGE)

            if (senderMessage.isNullOrBlank()) {

            } else {
                var messagePositionUD = senderMessage.lastIndexOf("UD-")
                var messageCompare = senderMessage.substring(messagePositionUD + 3)

                et_message.setText(messageCompare)
            }
        }
    }
}
