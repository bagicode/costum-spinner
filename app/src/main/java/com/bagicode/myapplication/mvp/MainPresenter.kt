package com.bagicode.myapplication.mvp

import com.bagicode.myapplication.api.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/*
 * Programmer by Robby Dianputra
 * MainPresenter adalah presenter dari page MainActivty
 *
 */

class MainPresenter(private val view: MainView,
                    private val apiService: ApiService) {

    fun getSpinnerFlag() {
        view.showLoading()
        apiService.getCountryCode()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view.showLoading() }
            .doOnComplete { view.hideLoading() }
            .doOnError { view.hideLoading()}
            .map { it.flags }
            .subscribe(
                { view.showFlagList(it) },
                { view.hideLoading() }
            )
    }

}