package com.bagicode.myapplication.mvp

import android.content.Intent
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.support.test.espresso.idling.CountingIdlingResource
import com.bagicode.myapplication.R
import com.bagicode.myapplication.SignupByPhoneActivity
import com.bagicode.myapplication.adapter.CountryAdapter
import com.bagicode.myapplication.api.ApiService
import com.bagicode.myapplication.model.CountryCodeItem

import java.util.ArrayList

/*
 * Programmer by Robby Dianputra
 * MainActivty adalah Home Page dari aplikasi ini
 * menggunakan arc MVP dan RX
 */

class MainActivity : AppCompatActivity(), MainView {

    private lateinit var presenter: MainPresenter
    private var mCountryList: ArrayList<CountryCodeItem>? = null
    private var mAdapter: CountryAdapter? = null

    private lateinit var idlingResource: CountingIdlingResource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initList()

        mAdapter = CountryAdapter(this, mCountryList!!)
        spinner_countries.setAdapter(mAdapter)

        spinner_countries.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val clickedItem = parent.getItemAtPosition(position) as CountryCodeItem
                val clickedCountryName = clickedItem.nicename

                Toast.makeText(this@MainActivity,
                    "$clickedCountryName selected", Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        })

        btn_register.setOnClickListener {
            val goSignupByPhone = Intent(this, SignupByPhoneActivity::class.java)
            startActivity(goSignupByPhone)
        }

    }

    private fun initList() {
        mCountryList = ArrayList()
        idlingResource = CountingIdlingResource("DATA_LOADER")

        val request = ApiService()

        presenter = MainPresenter(this, request)
        presenter.getSpinnerFlag()

    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showFlagList(data: List<CountryCodeItem>) {
        var i = 0
        for (i in data.indices ) {
            mCountryList?.add(
                CountryCodeItem(
                    data.get(i).flags,
                    data.get(i).id, data.get(i).iso, data.get(i).nicename, data.get(i).phonecode
                )
            )

        }

        idlingResource.decrement()
    }

}
