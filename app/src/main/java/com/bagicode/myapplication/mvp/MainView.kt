package com.bagicode.myapplication.mvp

import com.bagicode.myapplication.model.CountryCodeItem

/*
 * Programmer by Robby Dianputra
 * MainPresenter adalah view dari page MainActivty
 *
 * showLoading : show progressbar
 * hideLoading : hide progressbar
 * showFlagList : show data list flag country
 */

interface MainView {

    fun showLoading()
    fun hideLoading()
    fun showFlagList(data: List<CountryCodeItem>)

}