package com.bagicode.myapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import com.bagicode.myapplication.api.ApiService
import com.bagicode.myapplication.model.SignupByPhoneResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signup_by_phone.*

/*
 * Programmer by Robby Dianputra
 * SignupByPhone adalah page request OTP
 * menggunakan RX
 */

class SignupByPhoneActivity : AppCompatActivity() {

    private lateinit var apiService: ApiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_by_phone)

        apiService = ApiService.create()

        btn_send_message.setOnClickListener {

            var noHandphone = et_nomor_phone.text
            if (noHandphone.isEmpty()) {

            } else {
                apiService.setSignupByPhone("Robby Dianputra", "62", et_nomor_phone.text.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { progressBar.show() }
                    .doOnComplete { progressBar.hide() }
                    .doOnError { progressBar.hide() }
                    .map { it }
                    .subscribe(
                        { displayQuote(it) },
                        { displayError(it) }
                    )
            }

        }
    }

    private fun displayQuote(data: SignupByPhoneResponse) {

        if (data.status.equals("200")) {
            Snackbar.make(
                btn_send_message,
                "Waiting SMS ",
                Snackbar.LENGTH_LONG
            ).show()

            btn_send_message.isEnabled = false
            finish()

            val goSignupVerifPhone = Intent(this, SignupVerifPhoneActivity::class.java)
            startActivity(goSignupVerifPhone)

        } else {
            Snackbar.make(
                btn_send_message,
                "Send SMS Failed, "+data.messages,
                Snackbar.LENGTH_LONG
            ).show()

            btn_send_message.isEnabled = false
        }

    }

    private fun displayError(throwable: Throwable?) {
        Snackbar.make(
            btn_send_message,
            "Failed to display list quote. \n ${throwable?.localizedMessage}",
            Snackbar.LENGTH_LONG
        ).show()

        Log.v("tamvan", "ini dia errornya " + throwable?.localizedMessage)
    }

    private fun View.show() {
        this.visibility = View.VISIBLE
    }

    private fun View.hide() {
        this.visibility = View.GONE
    }
}
