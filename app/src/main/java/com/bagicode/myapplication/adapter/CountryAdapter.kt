package com.bagicode.myapplication.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.ahmadrosid.svgloader.SvgLoader
import com.bagicode.myapplication.R
import com.bagicode.myapplication.model.CountryCodeItem

import java.util.ArrayList

/*
 * Programmer by Robby Dianputra
 * Fungsi kelas : Kelas ini digunakan sebagai adapter dari custom spinner
 * Spinner untuk load flag atau bendera sesuai negara
 */

class CountryAdapter(context: Context, countryList: ArrayList<CountryCodeItem>) :
    ArrayAdapter<CountryCodeItem>(context, 0, countryList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    private fun initView(position: Int, convertViews: View?, parent: ViewGroup): View {
        var convertView = convertViews
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                R.layout.country_spinner_row, parent, false
            )
        }

        val imageViewFlag = convertView?.findViewById<ImageView>(R.id.image_view_flag)
        val textViewName = convertView?.findViewById<TextView>(R.id.text_view_name)

        val currentItem = getItem(position)

        if (currentItem != null) {
            SvgLoader.pluck()
                .with(context as Activity?)
                .setPlaceHolder(
                    R.drawable.shape_rectangle_gray,
                    R.drawable.shape_rectangle_gray
                )
                .load("https://dev.undangin.com/flags/"+currentItem.flags, imageViewFlag);

            textViewName?.setText(currentItem.phonecode)
        }

        return convertView!!
    }
}