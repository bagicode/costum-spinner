package com.bagicode.myapplication.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsManager
import android.telephony.SmsMessage
import android.util.Log

/*
 * Programmer by Robby Dianputra
 * Fungsi kelas : Kelas ini digunakan untuk mengambil informasi message secara broadcast.
 */

class SmsReceiver : BroadcastReceiver() {

    internal val sms = SmsManager.getDefault()

    val EXTRA_SMS_NO = "extra_sms_no"
    val EXTRA_SMS_MESSAGE = "extra_sms_message"

    override fun onReceive(context: Context, intent: Intent) {
        val bundle = intent.extras
        try {
            if (bundle != null) {
                val pdusObj = bundle.get("pdus") as Array<Any>
                for (i in pdusObj.indices) {
                    val currentMessage = getIncomingMessage(pdusObj[i], bundle)
                    val phoneNumber = currentMessage.displayOriginatingAddress
                    val message = currentMessage.displayMessageBody

                    if (message.isNullOrBlank()) {

                    } else {
                        var messagePositionUD = message.lastIndexOf("UD-")

                        if (messagePositionUD.equals(-1)){

                        } else {
                            val i = Intent("broadCastSMS")
                            i.putExtra(EXTRA_SMS_MESSAGE, message)
                            context.sendBroadcast(i)

                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.e("SmsReceiver", "Exception smsReceiver$e")
        }

    }

    private fun getIncomingMessage(aObject: Any, bundle: Bundle): SmsMessage {
        val currentSMS: SmsMessage
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val format = bundle.getString("format")
            currentSMS = SmsMessage.createFromPdu(aObject as ByteArray, format)
        } else {
            currentSMS = SmsMessage.createFromPdu(aObject as ByteArray)
        }
        return currentSMS
    }
}