package com.bagicode.myapplication.api

import com.bagicode.myapplication.BuildConfig
import com.bagicode.myapplication.model.CountryCodeResponse
import com.bagicode.myapplication.model.SignupByPhoneResponse
import io.reactivex.Flowable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

/*
 * Programmer by Robby Dianputra
 * Fungsi kelas : Kelas ini digunakan sebagai inisialisasi link API
 *
 * getCountryCode()
 * digunakan untuk mengambil list flag bendera dari semua negara
 *
 * setSignupByPhone()
 * digunakan untuk melakukan permintaan / request OTP
 *
 * ApiService()
 * digunakan sebagai link root utama
 */

interface ApiService {

    @GET("${BuildConfig.TSDB_API_KEY}/m/country_code_phone.php")
    fun getCountryCode()
            : Flowable<CountryCodeResponse>

    @FormUrlEncoded
    @POST("${BuildConfig.TSDB_API_KEY}/m/user/signupByPhone.php")
    fun setSignupByPhone(@Field("fullname") fullname:String,
                         @Field("countryCode") countryCode:String,
                         @Field("no_hp") no_hp:String)
            : Flowable<SignupByPhoneResponse>

    companion object Factory {
        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BuildConfig.BASE_URL)
                    .build()
            return retrofit.create(ApiService::class.java)
        }

        operator fun invoke(): ApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }

}